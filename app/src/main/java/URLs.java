public class URLs {
    private static final String ROOT_URL = "https://6249b46afd7e30c51c053219.mockapi.io/api/v1/";

    public static final String URL_REGISTER = ROOT_URL + "signup";
    public static final String URL_LOGIN= ROOT_URL + "login";
}
