package com.examplehadi.firstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.examplehadi.firstapp.model.GitHubRepo;
import com.examplehadi.firstapp.external.service.GitHubService;
import com.examplehadi.firstapp.activity.supporting.LoginActivity;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    TextInputLayout nameLayout, emailLayout, passwordLayout, contactLayout;
    EditText name, email, password, contact;
    MaterialButton clearAllButton;
    private ListView listView;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sp = getSharedPreferences("login",MODE_PRIVATE);
        if(sp.getBoolean("logged",false)){
            Log.d("V", "---------------------- logged== false");
//            sp.edit().putBoolean("logged",true).apply();
        }else{
            Log.d("V", "---------------------- logged== true");
//            sp.edit().putBoolean("logged",false).apply();
        }

        startActivity(new Intent(this, LoginActivity.class));


        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();

        GitHubService client = retrofit.create(GitHubService.class);
        Call<List<GitHubRepo>> call = client.reposForUser("presslabs");

        final Button button = findViewById(R.id.btn_resubmit);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                call.enqueue(new Callback<List<GitHubRepo>>() {
                    @Override
                    public void onResponse(Call<List<GitHubRepo>> call, Response<List<GitHubRepo>> response) {
                        List<GitHubRepo> repos =  response.body();
                        Toast.makeText(MainActivity.this,"Done :)",Toast.LENGTH_LONG).show();
//                        for (int i = 0 ; i < callList.size() ; i++)
//                            Log.d("value is" , callList.get(i).toString());
                        Log.d("V", String.valueOf(repos.size()));            }

                    @Override
                    public void onFailure(Call<List<GitHubRepo>> call, Throwable t) {
                        Toast.makeText(MainActivity.this,"Error :(",Toast.LENGTH_LONG).show();
                    }
                });

            }
        });




        //if the user is already logged in we will directly start the profile activity
//        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
//            finish();
//            Toast.makeText(this,"alreday loged",Toast.LENGTH_LONG).show();
////            startActivity(new Intent(this, ProfileActivity.class));
//            return;
//        }

//        Toast.makeText(this,"not loged in" +
//                "",Toast.LENGTH_LONG).show();
    }


        //Initializing Text Input Layouts
//        nameLayout = findViewById(R.id.name_layout);
//        emailLayout = findViewById(R.id.email_layout);
//        passwordLayout = findViewById(R.id.password_layout);
//        contactLayout = findViewById(R.id.contact_layout);
//
//        //Initializing EditTexts
//        name = findViewById(R.id.name);
//        email = findViewById(R.id.email);
//        contact = findViewById(R.id.contact);
//        password = findViewById(R.id.password);
//
//        //Initializing Button
//        clearAllButton = findViewById(R.id.clear_text_button);
//
//        //Clearing Text With Button
//        clearAllButton.setOnClickListener(view -> {
//            Toast.makeText(this,name.getText().toString(),Toast.LENGTH_SHORT).show();
//
//            //Clear Values
////            name.getText().clear();
////            email.getText().clear();
////            contact.getText().clear();
////            password.getText().clear();
////            //Clear Focus
////            contact.clearFocus();
////            email.clearFocus();
////            password.clearFocus();
//        });

        //Setting Error If User Enters Something Incorrect
//        name.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                //Checking
//                if (!charSequence.toString().isEmpty() && !charSequence.toString().matches("[a-zA-Z ]+")) {
//                    //Shows error when value entered is not empty & contain numbers
//                    nameLayout.setError("Allow only character");
//                } else {
//                    //Hide Error
//                    nameLayout.setError(null);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//    }
}