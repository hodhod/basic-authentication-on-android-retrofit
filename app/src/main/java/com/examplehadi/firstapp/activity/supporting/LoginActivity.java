package com.examplehadi.firstapp.activity.supporting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.examplehadi.firstapp.MainActivity;
import com.examplehadi.firstapp.R;
import com.examplehadi.firstapp.external.resource.UserResource;
import com.examplehadi.firstapp.external.service.client.Retrofit;
import com.examplehadi.firstapp.model.User;
import com.examplehadi.firstapp.external.resource.ErrorMessage;
import com.examplehadi.firstapp.external.resource.LoginResponse;
import com.examplehadi.firstapp.external.service.UserService;
import com.examplehadi.firstapp.model.UserFactory;
import com.google.gson.Gson;

import org.modelmapper.ModelMapper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private EditText etEmail, etPassword;
    SharedPreferences sp;
    private UserService userService = Retrofit.getService(UserService.class);

    ModelMapper modelMapper = new ModelMapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_login);

        sp = getSharedPreferences("login",MODE_PRIVATE);

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser();
            }
        });

        findViewById(R.id.registerLink).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }


    private void loginUser() {
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);//UI reference of textView

        final String email = etEmail.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();

        if (email.isEmpty()) {
            etEmail.setError("Email is required");
            etEmail.requestFocus();
            return;
        } else if (password.isEmpty()) {
            etEmail.setError("Password is required");
            etEmail.requestFocus();
            return;
        }

        UserResource user = new UserResource(
                null,
                email,
                password,
                null
        );

        doLogin(user);
    }


    private void doLogin(UserResource userResource){
        Call<LoginResponse> call = userService.login(
                userResource //modelMapper.map(user, UserResource.class)
        );
        call.enqueue(new LoginHandler());
    }

    private class LoginHandler implements Callback<LoginResponse> {
        @Override
        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
            if(response.isSuccessful()){
                Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                User user = UserFactory.get(response.body().user);

                /**
                 * When you go to teacher journey, you can do
                 * Teacher teacher = (Teacher) user
                 */

                // todo store the user in sp: SharedPreferences

                sp.edit().putBoolean("logged",true).apply();

                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
            else{
                Log.w("is Success => ","False");
                Gson gson = new Gson();
                ErrorMessage message=gson.fromJson(response.errorBody().charStream(),ErrorMessage.class);
                Toast.makeText(LoginActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<LoginResponse> call, Throwable t) {
            Log.d("onFailure", t.getMessage());
            Toast.makeText(LoginActivity.this,"Something Wrong!",Toast.LENGTH_SHORT);
        }
    }
}