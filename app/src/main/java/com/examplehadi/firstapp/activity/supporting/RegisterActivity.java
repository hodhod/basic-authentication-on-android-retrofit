package com.examplehadi.firstapp.activity.supporting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.examplehadi.firstapp.R;
import com.examplehadi.firstapp.external.resource.UserResource;
import com.examplehadi.firstapp.model.User;
import com.examplehadi.firstapp.external.resource.ErrorMessage;
import com.examplehadi.firstapp.external.resource.RegsiterResponse;
import com.examplehadi.firstapp.external.service.UserService;
import com.examplehadi.firstapp.external.service.client.Retrofit;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    TextInputLayout nameLayout, emailLayout, passwordLayout;
    EditText etName, etEmail, etPassword;
    MaterialButton btRegister;
    TextView tvLoginLink;
    AutoCompleteTextView tvRole;
    private UserService userService = Retrofit.getService(UserService.class);
    ModelMapper modelMapper = new ModelMapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // enable full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_register);

        initUI();

        // Validation
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Checking
                if (!charSequence.toString().isEmpty() && !charSequence.toString().matches("[a-zA-Z ]+")) {
                    //Shows error when value entered is not empty & contain numbers
                    nameLayout.setError("Allow only character");
                } else {
                    //Hide Error
                    nameLayout.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btRegister.setOnClickListener(view -> {
            UserResource user = new UserResource(
                    etName.getText().toString(),
                    etEmail.getText().toString(),
                    etPassword.getText().toString(),
                    tvRole.getText().toString()
            );
            doRegister(user);
        });

        tvLoginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });
    }


    private void initUI()
    {
        nameLayout = findViewById(R.id.name_layout);
        emailLayout = findViewById(R.id.email_layout);
        passwordLayout = findViewById(R.id.password_layout);

        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        tvRole = findViewById(R.id.tvRole);

        // create list of roles
        List<String> roleList = getRoleList();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(RegisterActivity.this, android.R.layout.simple_spinner_item, roleList);
        tvRole.setAdapter(adapter);

        btRegister = findViewById(R.id.btRegister);

        tvLoginLink = findViewById(R.id.tvLoginLink);
    }

    private List<String> getRoleList()
    {
        return Arrays.asList("Admin", "Doctor", "Pharmacist", "Patient");
    }

    private void doRegister(UserResource userResource) {
        Call<RegsiterResponse> call = userService.register(
                userResource //modelMapper.map(user, UserResource.class)
        );
        call.enqueue(new RegisterHandler());
    }

    class RegisterHandler implements Callback<RegsiterResponse> {
        @Override
        public void onResponse(Call<RegsiterResponse> call, Response<RegsiterResponse> response) {
            if(response.isSuccessful()){
                Toast.makeText(RegisterActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
            else{
                Log.w("is Success => ","False");
                Gson gson = new Gson();
                ErrorMessage message=gson.fromJson(response.errorBody().charStream(),ErrorMessage.class);
                Toast.makeText(RegisterActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<RegsiterResponse> call, Throwable t) {
            Log.d("onFailure", t.getMessage().toString());
            Toast.makeText(RegisterActivity.this,"Something Wrong!",Toast.LENGTH_SHORT);
        }
    }
}