package com.examplehadi.firstapp.model;

public class Doctor implements User{

    private Integer id;
    private String name;
    private String email;

    public Doctor(Integer id, String name, String email) {
        this.id = id ;
        this.name = name;
        this.email = email;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public UserType getType() {
        return UserType.Doctor;
    }
}
