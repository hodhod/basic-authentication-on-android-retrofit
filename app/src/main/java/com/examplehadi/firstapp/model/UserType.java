package com.examplehadi.firstapp.model;

public enum UserType {
    Doctor,
    Patient
}
