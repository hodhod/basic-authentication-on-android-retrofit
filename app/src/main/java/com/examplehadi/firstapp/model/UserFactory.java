package com.examplehadi.firstapp.model;

import com.examplehadi.firstapp.external.resource.UserResource;

public class UserFactory {
    public static User get(UserResource userResource) {
        switch (userResource.getRole()) {
            case "doctor":
                new Doctor(userResource.getId(), userResource.getName(), userResource.getEmail());
            default:
                throw new IllegalArgumentException("not implemented yet");
        }
    }
}
