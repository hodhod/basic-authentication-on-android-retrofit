package com.examplehadi.firstapp.model;

public interface User {
    Integer getId ();
    String getName();
    String getEmail();
    UserType getType();
}
