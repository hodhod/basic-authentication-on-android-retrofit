package com.examplehadi.firstapp.external.service.client;

import com.examplehadi.firstapp.external.service.Service;

import retrofit2.converter.gson.GsonConverterFactory;

public class Retrofit {

    private static  final String BASE_URL = "http://192.168.0.100:3000/";
    private static retrofit2.Retrofit retrofit;

    public static synchronized retrofit2.Retrofit getInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static <T extends Service> T getService(final Class<T> service) {
        return Retrofit.getInstance().create(service);
    }

//    public API getAPI () {
//        return retrofit.create(API.class);
//    }

}
