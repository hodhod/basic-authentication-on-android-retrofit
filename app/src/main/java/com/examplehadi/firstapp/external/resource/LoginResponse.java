package com.examplehadi.firstapp.external.resource;

import com.examplehadi.firstapp.model.User;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("user")
    public UserResource user;

    @SerializedName("msg")
    public String msg;

    @SerializedName("token")
    public String token;


    public String getMsg() {
        return msg;
    }

    public UserResource getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }
}
