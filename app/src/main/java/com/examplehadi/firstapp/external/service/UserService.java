package com.examplehadi.firstapp.external.service;

import com.examplehadi.firstapp.external.resource.UserResource;
import com.examplehadi.firstapp.external.resource.LoginResponse;
import com.examplehadi.firstapp.external.resource.RegsiterResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService extends Service {

    @POST("user/signup")
    Call<RegsiterResponse> register(@Body UserResource user);

    @POST("/user/login")
    Call<LoginResponse> login(@Body UserResource user);

}
