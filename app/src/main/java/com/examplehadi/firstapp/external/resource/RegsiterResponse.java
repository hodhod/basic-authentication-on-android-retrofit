package com.examplehadi.firstapp.external.resource;

import com.examplehadi.firstapp.model.User;
import com.google.gson.annotations.SerializedName;

public class RegsiterResponse {
    @SerializedName("user")
    public User user;

    @SerializedName("msg")
    public String msg;


    public String getMsg() {
        return msg;
    }

    public User getUser() {
        return user;
    }

}
